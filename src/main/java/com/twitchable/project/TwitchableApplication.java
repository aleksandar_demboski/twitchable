package com.twitchable.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwitchableApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwitchableApplication.class, args);
	}
}
