'use strict';

/**
 * @ngdoc function
 * @name twitchableFrontEndApp.controller:RandomchannelsCtrl
 * @description
 * # RandomchannelsCtrl
 * Controller of the twitchableFrontEndApp
 */
angular.module('twitchableFrontEndApp')
  .controller('ShowSearchResultsCtrl', function (SearchResultsService) {

	  this.channels = SearchResultsService.getChannels();
    
  });
