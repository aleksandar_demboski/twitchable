'use strict';

/**
 * @ngdoc overview
 * @name twitchableFrontEndApp
 * @description
 * # twitchableFrontEndApp
 *
 * Main module of the application.
 */
angular
  .module('twitchableFrontEndApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngLoadingSpinner',
    'googlechart',
    'ngTable'
  ])
  .config(function ($routeProvider, $sceDelegateProvider) {
    $routeProvider
    .when('/video', {
  	  templateUrl: 'viewstream',
  	  controller: 'ViewstreamCtrl',
  	  controllerAs: 'viewstream'
    })
      .when('/search', {
        templateUrl: 'searchresults',
        controller: 'ShowSearchResultsCtrl',
        controllerAs: 'searchresults'
      })
      .when('/testgraph', {
        templateUrl: 'testgraph',
        controller: 'TestGraphCtrl',
        controllerAs: 'testgraph'
      })
      .when('/', {
        templateUrl: 'randomchannels',
        controller: 'RandomchannelsCtrl',
        controllerAs: 'randomchannels'
      })
      .otherwise({
        redirectTo: '/'
      });
    $sceDelegateProvider.resourceUrlWhitelist(['self', 'https://player.twitch.tv/**', 'https://www.twitch.tv/**']);
  });
