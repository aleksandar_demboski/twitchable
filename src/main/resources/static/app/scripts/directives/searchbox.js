'use strict';


angular.module('twitchableFrontEndApp')
  .directive('searchbox', function () {
	  return {
			restrict : 'E',
			template : '<div id="filter-panel" class="collapse filter-panel">'
      +'<div style="padding-left: 35%; padding-top: 18px; padding-bottom:13px">'
          +'<form class="form-inline" role="form">'
              +'<div class="form-group">'
                  +'<label for="pref-perpage" style="color: white">Rating:</label>'
              +'<select ng-model="rating" id="pref-perpage" class="form-control">'
                  +'<option value="1">1 star</option>'
                  +'<option value="2">2 stars</option>'
                  +'<option value="3">3 stars</option>'
                  +'<option value="4">4 stars</option>'
                  +'<option value="5">5 stars</option>'
              +'</select>'                               
              +'</div>'
             + '<div class="form-group" style="margin-left: 20px">'
                 + '<label for="language" style="color: white">Language:</label>'
             + '<select ng-model="broadcasterLanguage" id="language" class="form-control">'
                  +'<option value="1">English</option>'
                  +'<option value="2">German</option>'
                  +'<option value="3">French</option>'
              +'</select>'                             
              +'</div>'
              +'<div class="form-group" style="margin-left: 20px">'
                  +'<label for="game" style="color: white">Game:</label>'
              +'<select ng-model="game" id="game" class="form-control">'
                  +'<option value="Game 1">Game 1</option>'
                  +'<option value="2">Game 2</option>'
                  +'<option value="3">Game 3</option>'
              +'</select>'                                
             +' </div>'
          +'</form>'
      +'</div>'
 +' </div>'
+'<form style="margin-left: 40%" class="navbar-form navbar-left" role="search">'
  +'<div class="form-group">'
      +'<input type="text" ng-model="name" class="form-control" id="navbar-search-input" placeholder="Search"/>'
  +'</div>'
  +'<button class="btn btn-info btn-flat" ng-click="searchFor(query)">Search</button>'
+'</form>'
+'<ul class="nav navbar-nav">'
 +'<li>'
     +'<a data-toggle="collapse" ng-click="clearFilter()" data-target="#filter-panel" href="">Filter</a>'
  +'</li>'
+'</ul>'
+'<div class="navbar-custom-menu">'
	+'<ul class="nav navbar-nav">'
		+'<li ng-model="loggedinUser.data" ng-if="loggedinUser.data ==' + "''" + '">'
			+'<a href="/login/twitch">'
				+'Login'
			+'</a>'
		+'</li>'
		+ '<li ng-model="loggedinUser.data" ng-if="loggedinUser.data !=' + "''" + '" class="dropdown user user-menu">'
			+ '<a href="" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'
				+ '<img ng-if="loggedinUser.data.logo != ' + "'null'" + '" class="user-image" alt="User Image" />'
				+ '<img ng-src="https://image.freepik.com/free-icon/user-male-shape-in-a-circle--ios-7-interface-symbol_318-35357.png" ng-if="loggedinUser.data.logo == ' + "'null'" + '" class="user-image" alt="User Image" />'
				+ '<span class="hidden-xs">{{loggedinUser.data.displayName}}</span>'
			+ '</a>'
			+ '<ul class="dropdown-menu">'
				+ '<li class="user-header">'
				+ '<img ng-if="loggedinUser.data.logo != ' + "'null'" + '" class="img-circle" alt="User Image" />'
				+ '<img ng-src="https://image.freepik.com/free-icon/user-male-shape-in-a-circle--ios-7-interface-symbol_318-35357.png" ng-if="loggedinUser.data.logo == ' + "'null'" + '" class="img-circle" alt="User Image" />'
					+ '<p>'
						+ '{{loggedinUser.data.displayName}}'
						+ '<small>'
							+ 'Member since: {{loggedinUser.data.createdAt|date:"MMMM yyyy"}}'
						+'</small>'
					+'</p>'
				+'</li>'
				+ '<li class="user-footer">'
					+ '<div class="pull-left">'
						+ '<a ng-href="http://www.twitch.com/{{loggedinUser.data.name}}/profile" class="btn btn-default btn-flat">Profile' +'</a>'
					+'</div>'
					+ '<div class="pull-right">'
					+ '<a href="" ng-click="logoutUser()" class="btn btn-default btn-flat">Log out' +'</a>'
				+'</div>'
				+'</li>'
			+ '</ul>'
		+ '</li>'
	+'</ul>'
+'</div>',

			controller: ['$scope','$http','SearchResultsService', '$location', function($scope, $http, SearchResultsService, $location){
				
					$scope.name = null;
					$scope.game = null;
					$scope.rating = null;
					$scope.broadcasterLanguage = null;
				
					 $http.get("/user/login")
					    .then(function(response) {
					        $scope.loggedinUser = response;
					    });
					
					$scope.clearFilter = function(){
						
						$scope.game = null;
						$scope.rating = null;
						$scope.broadcasterLanguage = null;
						
					}
					
					$scope.logoutUser = function(){
						$http.get("/user/logout")
					    .then(function() {
					        $scope.loggedinUser.data = "";
					    });
					}
					
					$scope.searchFor = function(){
						
						var query = {
								channelProp: '',
								name: $scope.name,
								game: $scope.game,
								rating: $scope.rating,
								broadcasterLanguage: $scope.broadcasterLanguage
								
						}
						var d=JSON.stringify(query);
						$http({
					          method  : 'POST',
					          dataType: 'json',
					          url     : '/channel/searchJSON',
					          data    :  d,
					          headers : {'Content-Type': 'application/json'} 
					         })
				          .then(function (response) {
				        	  SearchResultsService.setChannels(response.data);
				        	  $location.path("/search")
				        	  
				          });
					}
				}
			             
			             
			]
	  
					
			
		};
  });
